# Goal Charts #

Projeto para automatizar a exportação de gráficos do Goal

### Pré-requisitos ###

- Visual Studio 2015
- Mozilla Firefox

### Instruções ###

1. Faça o download do projeto;
2. Duplique o arquivo App.config.example e renomear para App.config;
3. Substitua o valor das variáveis "goalUser" e "goalPassword" no arquivo App.config de acordo com seus dados de acesso;
4. No menu do Visual Studio, selecione Test > Run > All Tests (ou CTRL + R, A);
5. Aguarde a finalização de todos os testes;
6. Dentro do diretório do Projeto acesse GoalCharts/Images;
7. Nesta pasta haverão duas outras pastas (This Week Review e Velue Expectation) com as imagens dos gráficos;

OBS: Não é recomendada a execução usando a VPN do GAB.

Boa Weekly!

### Erros / Melhorias ###

Por se tratar de um projeto em versão Beta, a aplicação está sujeita a erros.
Correção de defeitos e melhorias são muito bem-vindas.

### Autor ###
Gabriel Barbosa <gabrielbr@ciandt.com>