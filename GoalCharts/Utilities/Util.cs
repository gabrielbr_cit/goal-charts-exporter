﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using static System.Net.Mime.MediaTypeNames;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using GoalCharts.Classes;
using System.Collections.Generic;
using System.Linq;

namespace GoalCharts.Utilities
{
    public static class Util
    {

        public static string GetBaseUrl()
        {
            return ConfigurationSettings.AppSettings["goalURL"].ToString();
        }

        public static string GetUsername()
        {
            return ConfigurationSettings.AppSettings["goalUser"].ToString();
        }

        public static void CaptureElementScreenShot(IWebDriver driver, string cssSelector, string filename, int widthToRemove, int heightToRemove, BoardEnum board)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor) driver;
            js.ExecuteScript("window.scrollTo(0, 0)");

            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            Rectangle rectangule = new Rectangle();

            var element = driver.FindElement(By.CssSelector(cssSelector));
            if (element != null)
            {
                int width = element.Size.Width - 20;
                int height = element.Size.Height - 20;
                Point point = element.Location;
                rectangule = new Rectangle(point.X, point.Y, (width - widthToRemove), (height-heightToRemove));
            }
            
            Bitmap image = new Bitmap(new MemoryStream(screenshot.AsByteArray));
            var elementImage = image.Clone(rectangule, image.PixelFormat);
            image.Dispose();

            string folder = string.Empty;
            if (board == BoardEnum.THIS_WEEK_REVIEW)
                folder = "This Week Review\\";
            else
                folder = "Value Expectation\\";

            var path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            var completePath = path + "\\Images\\" + folder;
            if (!Directory.Exists(completePath))
            {
                Directory.CreateDirectory(completePath);
            }

            filename = path + "\\Images\\" + folder + GetValidFilename(filename) + ".jpg";
            elementImage.Save(filename, ImageFormat.Jpeg);            
            elementImage.Dispose();
        }

        public static string GetValidFilename(string filename)
        {
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                filename = filename.Replace(c, '_');
            }
            return filename;
        }

        public static DateTime ToDateTime(string date, string format)
        {
            return DateTime.ParseExact(date, format, null);
        }

        public static Dictionary<String, DateTime> GetRangeSprints()
        {
            Dictionary<String, DateTime> ranges = new Dictionary<String, DateTime>();

            Dictionary<String, DateTime> thisWeekRange          = GetSprintRange(BoardEnum.THIS_WEEK_REVIEW);
            Dictionary<String, DateTime> valueExpectationRange  = GetSprintRange(BoardEnum.VALUE_EXPECTATION);

            ranges.Add("twInitial", thisWeekRange["initial"]);
            ranges.Add("twFinal", thisWeekRange["final"]);
            ranges.Add("veInitial", valueExpectationRange["initial"]);
            ranges.Add("veFinal", valueExpectationRange["final"]);

            return ranges;
        }

        private static Dictionary<String, DateTime> GetSprintRange(BoardEnum board, int maxSprints = 5)
        {
            Dictionary<String, DateTime> range = new Dictionary<String, DateTime>();
            Sprint firstSprint = null;
            Sprint lastSprint = null;

            if (board == BoardEnum.THIS_WEEK_REVIEW)
            {
                firstSprint = SprintList.Instance[SprintList.Instance.Count - 2];
                lastSprint  = SprintList.Instance[SprintList.Instance.Count - 1];

            }
            else if (board == BoardEnum.VALUE_EXPECTATION)
            {
                lastSprint = SprintList.Instance[SprintList.Instance.Count - 2];
                firstSprint = SprintList.Instance[(SprintList.Instance.Count - 1) - maxSprints];
            }

            range.Add("initial", firstSprint.InitialDate);
            range.Add("final", lastSprint.FinalDate);

            return range;
        }

        public static void DeleteAllFromDirectory(string directory)
        {
            DirectoryInfo di = new DirectoryInfo(directory);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }
    }
}
