﻿#language: pt-BR

Funcionalidade: Exportar os gráficos
                Eu, como usuário
                Desejo entrar no sistema Goal, me autenticar e exportar os gráficos.

Contexto:
	Dado que eu loguei com sucesso 
	E coletei todas as informações das sprints
	E acessei a página de Project Cards e selecionei o projeto

	Cenário: Exportar gráficos de previsibilidade
	Dado que selecionei previsibilidade
	Então o sistema deverá exportar os gráficos

	Cenário: Exportar os gráficos de qualidade
	Dado que selecionei qualidade
	Então o sistema deverá exportar os gráficos

	Cenário: Exportar os gráficos de produtividade
	Dado que selecionei produtividade
	Então o sistema deverá exportar os gráficos