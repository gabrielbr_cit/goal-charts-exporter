﻿#language: pt-BR

Funcionalidade: Limpar o diretório de exportação
                Eu, como usuário
                Desejo excluir as imagens exportadas na última execução.

Cenário: Excluir imagens do dirétório de exportação
Dado que existe o diretório de Imagens
Então deve-se remover todas as imagens contidas neles