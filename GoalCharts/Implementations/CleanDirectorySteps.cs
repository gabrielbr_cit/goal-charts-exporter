﻿using GoalCharts.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace GoalCharts.Implementations
{
    [Binding]
    public class LimparODiretorioDeExportacaoSteps : StepBase
    {
        [Given(@"que existe o diretório de Imagens")]
        public void DadoQueExisteODiretoriosDeImagens()
        {
            var path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            var completePath = path + "\\Images\\";
            if (Directory.Exists(completePath))
            {
                Util.DeleteAllFromDirectory(completePath);
            }
            
        }
        
        [Then(@"deve-se remover todas as imagens contidas neles")]
        public void EntaoDeve_SeRemoverTodasAsImagensContidasNeles()
        {
            Assert.AreEqual(1, 1);
        }
    }
}
