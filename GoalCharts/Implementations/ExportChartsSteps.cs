﻿using GoalCharts.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using GoalCharts.Classes;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace GoalCharts.Implementations
{
    [Binding]
    public class ExportarOsGraficosSteps : StepBase
    {
        [Given(@"que eu loguei com sucesso")]
        public void DadoQueEuLogueiComSucesso()
        {
            Login();            
        }

        [Given(@"coletei todas as informações das sprints")]
        public void EntaoColeteiTodasAsInformacoesDasSprints()
        {
            Driver.SwitchTo().Frame("ProjectRoadmapFrame");

            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            int length = int.Parse(js.ExecuteScript("return document.querySelector('.sprints-container').children.length").ToString()) - 1;

            List<Sprint> sprintList = SprintList.Instance;
            for (int i = 0; i < length; i++)
            {
                string period = FindElement(".sprints-container .sprint-" + i + " .sprint .period", 60).Text;
                Sprint sprint = new Sprint();
                sprint.Index = i;
                sprint.Name = FindElement(".sprints-container .sprint-" + i + " .sprint .name", 60).Text;
                sprint.InitialDate = GetInitialDateFromPeriod(period);
                sprint.FinalDate = GetFinalDateFromPeriod(period);

                if(!sprint.Name.Contains("*"))
                    SprintList.Add(sprint);
            }

        }

        [Given(@"acessei a página de Project Cards e selecionei o projeto")]
        public void EAcesseiAPaginaDeProjectCardsESelecioneiOProjeto()
        {
            Driver.Url = Util.GetBaseUrl() + "GoalReports.aspx?Mode=Cards";
            Thread.Sleep(3000);
            FindElement("#ctl00_selProjects ~ div > button", 30).Click();
            FindElement("#ctl00_selProjects ~ div > ul > li:nth-child(2) > a > label > input", 30).Click();
            FindElement("#ctl00_selProjects ~ div > button", 30).Click();
            FindElement("#cardsContainer > .card:nth-child(1)", 30).Click();
            Thread.Sleep(2000);
        }

        [Given(@"que selecionei previsibilidade")]
        public void DadoQueSelecioneiPrevisibilidade()
        {
            Dictionary<String, DateTime> ranges = Util.GetRangeSprints();
            GenerateChart((int) DashboardEnum.PROJECT, 26, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int) DashboardEnum.PROJECT, 26, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);
        }
        
        [Given(@"que selecionei qualidade")]
        public void DadoQueSelecioneiQualidade()
        {
            Dictionary<String, DateTime> ranges = Util.GetRangeSprints();

            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 28, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 28, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);

            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 30, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 30, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);

            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 32, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 32, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);

            GenerateChart((int)DashboardEnum.PROJECT, 34, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 4);
            GenerateChart((int)DashboardEnum.PROJECT, 34, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 4);

            GenerateChart((int)DashboardEnum.DEVELOPMENT_PHASE, 2, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 4);
            GenerateChart((int)DashboardEnum.DEVELOPMENT_PHASE, 2, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 4);
        }
        
        [Given(@"que selecionei produtividade")]
        public void DadoQueSelecioneiProdutividade()
        {
            Dictionary<String, DateTime> ranges = Util.GetRangeSprints();

            GenerateChart((int)DashboardEnum.PROJECT, 30, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 999, 8000);
            GenerateChart((int)DashboardEnum.PROJECT, 30, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 999, 8000);

            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 34, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 34, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);

            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 38, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 38, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);

            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 40, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.KPIS_AND_SLAS, 40, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);

            GenerateChart((int)DashboardEnum.WORKLOG, 2, ranges["twInitial"], ranges["twFinal"], BoardEnum.THIS_WEEK_REVIEW, 2);
            GenerateChart((int)DashboardEnum.WORKLOG, 2, ranges["veInitial"], ranges["veFinal"], BoardEnum.VALUE_EXPECTATION, 2);
        }
        
        [Then(@"o sistema deverá exportar os gráficos")]
        public void EntaoOSistemaDeveraExportarOsGraficosDeQualidade()
        {
            Assert.AreEqual(Util.GetUsername(), FindElement("#ctl00_lblexibeuser", 40).Text.Trim());
        }

        private DateTime GetInitialDateFromPeriod(string period)
        {
            if (period.Length != 23)
                return DateTime.Now;

            return Util.ToDateTime(period.Substring(0, 10), "MM/dd/yyyy");
        }

        private DateTime GetFinalDateFromPeriod(string period)
        {
            if (period.Length != 23)
                return DateTime.Now;

            return Util.ToDateTime(period.Substring(13, 10), "MM/dd/yyyy");
        }
    }
}
