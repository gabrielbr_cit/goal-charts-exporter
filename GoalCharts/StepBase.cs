﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Configuration;
using GoalCharts.Utilities;
using System.Diagnostics;
using GoalCharts.Classes;

namespace GoalCharts
{
    public class StepBase
    {
        public IWebDriver Driver
        {
            get
            {
                return _driver;
            }
        }

        private IWebDriver _driver = new FirefoxDriver();

        public void WaitForExists(string cssSelector, int timeout = 60)
        {
            WaitFor(ExpectedConditions.ElementExists(By.CssSelector(cssSelector)), timeout);
        }

        public void WaitForVisible(string cssSelector, int timeout = 60)
        {
            WaitFor(ExpectedConditions.ElementIsVisible(By.CssSelector(cssSelector)), timeout);
        }

        public void WaitFor<TResult>(Func<IWebDriver, TResult> condition, int timeout = 60)
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(timeout)).Until(condition);
        }

        public void DoubleClick(string cssSelector, int timeout = 0)
        {
            if (timeout > 0)
            {
                WaitForExists(cssSelector, timeout);
            }
            ((IJavaScriptExecutor)_driver).ExecuteScript("$('" + cssSelector + "').dblclick()");
        }

        public IWebElement FindElement(string cssSelector, int timeout = 0)
        {
            if (timeout > 0)
            {
                WaitForExists(cssSelector, timeout);
            }
            return Driver.FindElement(By.CssSelector(cssSelector));
        }

        public void Click(string cssSelector, int timeout = 0)
        {
            if (timeout > 0)
            {
                WaitForExists(cssSelector, timeout);
            }
            ((IJavaScriptExecutor)_driver).ExecuteScript("$('" + cssSelector + "').click()");
        }

        public void Login()
        {
            string url = ConfigurationSettings.AppSettings["goalUrl"];
            string username = ConfigurationSettings.AppSettings["goalUser"];
            string password = ConfigurationSettings.AppSettings["goalPassword"];

            Driver.Url = url;
            FindElement("#txt_login", 60).SendKeys(username);
            FindElement("#txt_pass").SendKeys(password);
            FindElement("#bt_login").Click();
            
            Thread.Sleep(5000); // aguarda 5s para página carregar completamente
        }

        public void GenerateChart(int dashboard, int report, DateTime initial, DateTime final, BoardEnum board, int groupBy = 999, int extraDelay = 0)
        {
            try
            {
                Thread.Sleep(2000);

                SelectOptionFromChosen("#ctl00_cphContent_selDashboard_chosen", dashboard);
                Thread.Sleep(2000);

                SelectOptionFromChosen("#ctl00_cphContent_selReport_chosen", report);
                Thread.Sleep(2000);

                FindElement("#date_from").Clear();
                FindElement("#date_from").SendKeys(initial.ToString("MM/dd/yyyy"));
                FindElement("#date_to").Clear();
                FindElement("#date_to").SendKeys(final.ToString("MM/dd/yyyy"));

                if (groupBy != 999)
                {
                    SelectOptionFromChosen("#ctl00_cphContent_ddlGroupBy_chosen", groupBy);
                    Thread.Sleep(2000);
                }

                FindElement("#btGenerate", 30).Click();
                if (extraDelay > 0)
                    Thread.Sleep(extraDelay);
                Thread.Sleep(15000);

                int tableViewHeight = FindElement("#tableview", 30).Size.Height;
                string chartTitle = FindElement("#titleChart", 30).Text;
                Util.CaptureElementScreenShot(Driver, "#chartContainer", chartTitle, 0, tableViewHeight, board);
                Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
        }

        private void SelectOptionFromChosen(string cssSelector, int option)
        {
            bool isDropdownOpened = false;
            while (!isDropdownOpened)
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
                js.ExecuteScript("$('" + cssSelector + " > .chosen-drop > .chosen-results').css('max-height', '2000px')");

                FindElement(cssSelector + " > a", 60).Click();
                isDropdownOpened = FindElement(cssSelector).GetAttribute("class").Contains("chosen-with-drop");
            }
            Thread.Sleep(1000);
            FindElement(cssSelector + " > .chosen-drop > .chosen-results > li[data-option-array-index='" + option + "']", 120).Click();
        }
    }
}