﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoalCharts.Classes
{
    public class Sprint
    {
        private int index;
        private string name;
        private DateTime initialDate;
        private DateTime finalDate;

        public Sprint(int index, string name, DateTime initialDate, DateTime finalDate)
        {
            this.index = index;
            this.name = name;
            this.initialDate = initialDate;
            this.finalDate = finalDate;
        }

        public Sprint()
        { }

        public int Index
        {
            get
            {
                return index;
            }

            set
            {
                index = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public DateTime InitialDate
        {
            get
            {
                return initialDate;
            }

            set
            {
                initialDate = value;
            }
        }

        public DateTime FinalDate
        {
            get
            {
                return finalDate;
            }

            set
            {
                finalDate = value;
            }
        }
    }
}
