﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoalCharts.Classes
{
    public enum BoardEnum
    {
        VALUE_EXPECTATION = 1,
        THIS_WEEK_REVIEW  = 2
    }
}
