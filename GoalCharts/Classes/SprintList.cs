﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoalCharts.Classes
{
    public class SprintList
    {

        private static List<Sprint> _instance;

        private SprintList() {
            _instance = new List<Sprint>();
        }

        public static List<Sprint> Instance
        {
            get
            {
                if (_instance == null)
                {
                    new SprintList();
                }
                return _instance;
            }
        }

        public static void Add(Sprint sprint)
        {
            _instance.Add(sprint);
        }

    }
}
