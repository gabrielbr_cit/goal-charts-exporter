﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoalCharts.Classes
{
    public enum DashboardEnum
    {
        AFFECT_VERSION = 0,
        ANDON = 2,
        ASSIGNEE = 4,
        COMPONENTS = 6,
        CURRENT_STATUS = 8,
        DEVELOPMENT_PHASE = 10,
        FIX_VERSION = 12,
        GROUPED_COMPONENT = 14,
        ISSUE_TYPES = 16,
        KPIS_DEEP_DIVE = 18,
        KPIS_AND_SLAS = 20,
        LABEL = 22,
        LEVEL = 24,
        PROJECT = 26,
        RESOLUTION = 28,
        ROOT_CAUSE_AND_PROBLEM = 30,
        SERVICE_EVALUATION = 32,
        SEVERITY = 34,
        TABLE = 36,
        WORKLOG = 38
    }
}
